﻿using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EFDbInitializer : IDbInitializer
    {
        private DataContext _dataContext;

        public EFDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Init()
        {
            //_dataContext.Database.EnsureDeleted();
            //_dataContext.Database.EnsureCreated();
            _dataContext.Database.Migrate();
            _dataContext.Preferences.AddRange(FakeDataFactory.Preferences);
            _dataContext.SaveChangesAsync();

            _dataContext.Customers.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChangesAsync();

            _dataContext.Employees.AddRange(FakeDataFactory.Employees);
            _dataContext.SaveChangesAsync();
        }
    }
}
