﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;

        public CustomersController(IRepository<Customer> customerRepository, IRepository<CustomerPreference> customerPreference)
        {
            _customerRepository = customerRepository;
            _customerPreferenceRepository = customerPreference;

        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            var response = customers.Select(x => new CustomerShortResponse()
            {
                Email = x.Email,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Id = x.Id
            }).ToList();
            return Ok(response);
        }

        /// <summary>
        /// Получить данные клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            var response = new CustomerResponse(customer);
            return Ok(response);
        }

        /// <summary>
        /// Создать нового клиента
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            Customer customer = CustomerMapper.MapNewCustomer(request);
            await _customerRepository.AddAsync(customer);
            return Ok(customer);
        }

        /// <summary>
        /// Изменить данные клиента
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var test = _customerPreferenceRepository.GetAllAsync().Result.Where(x => x.CustomerId == id);            
            await _customerPreferenceRepository.DeleteManyAsync(test);
            var customer = await _customerRepository.GetByIdAsync(id);
            Customer updatedCustomer = CustomerMapper.MapNewCustomer(request, customer);
            await _customerRepository.UpdateAsync(updatedCustomer);
            return Ok(updatedCustomer);
        }

        /// <summary>
        /// Удалить клиента по id
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();
            await _customerRepository.DeleteAsync(customer);
            return NoContent();
        }
    }
}