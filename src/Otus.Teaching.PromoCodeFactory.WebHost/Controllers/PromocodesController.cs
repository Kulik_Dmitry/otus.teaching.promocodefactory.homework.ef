﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Mappers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository, IRepository<Employee> employeeRepository,
            IRepository<Preference> preferenceRepository,IRepository<CustomerPreference> customerPreferenceRepository)
            {
                _promoCodeRepository = promoCodeRepository;
                _employeeRepository = employeeRepository;
                _preferenceRepository = preferenceRepository;
                _customerPreferenceRepository = customerPreferenceRepository;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();
            var response = promoCodes.Select(pc => pc.GetResponse()).ToList();
            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var employes = await _employeeRepository.GetAllAsync();
            var currentEmployee = employes.SingleOrDefault(e => e.FullName.Equals(request.PartnerName));
            var preferences = await _preferenceRepository.GetAllAsync();
            var currentPreference = preferences.SingleOrDefault(p => p.Name.Equals(request.Preference));
            if (currentEmployee == null || currentPreference == null)
                return ValidationProblem();
            var customerPreferences = await _customerPreferenceRepository.GetAllAsync();
            var customerIds = customerPreferences.Where(cp => cp.PreferenceId == currentPreference.Id).Select(x => x.CustomerId);
            PromoCode promoCode = null;
            foreach(var id in customerIds)
            {
                promoCode = PromoCodeMapper.Create(request, currentPreference, currentEmployee, id);
                await _promoCodeRepository.AddAsync(promoCode);
            }
            return Ok();
        }
    }
}