﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class CustomerMapper
    {
        public static Customer MapNewCustomer(CreateOrEditCustomerRequest request, Customer customer = null)
        {
            if (customer == null)
            {
                customer = new Customer();
                customer.Id = Guid.NewGuid();
            }
            customer.LastName = request.LastName;
            customer.FirstName = request.FirstName;
            customer.Email = request.Email;
            customer.Preferences = request.PreferenceIds.Select(preferenceId => new CustomerPreference()
            {
                Id = Guid.NewGuid(),
                CustomerId = customer.Id,
                PreferenceId = preferenceId
            }).ToList();
            return customer;
        }
    }
}
