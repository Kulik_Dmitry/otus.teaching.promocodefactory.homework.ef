﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mappers
{
    public static class PromoCodeMapper
    {
        public static PromoCode Create(GivePromoCodeRequest request, Preference preference, Employee employee, Guid customerId)
        {
            return new PromoCode()
            {
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                Code = request.PromoCode,
                PartnerManagerId = employee.Id,                
                PreferenceId = preference.Id,                
                CustomerId = customerId
            };
        }

        public static PromoCodeShortResponse GetResponse(this PromoCode promoCode) =>
            new PromoCodeShortResponse()
            {
                BeginDate = promoCode.BeginDate.ToShortDateString(),
                Code = promoCode.Code,
                EndDate = promoCode.EndDate.ToShortDateString(),
                Id = promoCode.Id,
                PartnerName = promoCode.PartnerName,
                ServiceInfo = promoCode.ServiceInfo
            };
    }
}
