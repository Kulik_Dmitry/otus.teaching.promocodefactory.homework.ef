﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PrefernceResponse
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        public PrefernceResponse(Preference preference)
        {
            Id = preference.Id;
            Name = preference.Name;
        }
    }
}
